package com.hiber;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
@Entity
public class Answer {
	@Id
	int aid;
	public String ans;
	@OneToOne
	public Question question;
	public Answer() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Answer(int aid, String ans, Question question) {
		super();
		this.aid = aid;
		this.ans = ans;
		this.question = question;
	}
	public int getAid() {
		return aid;
	}
	public void setAid(int aid) {
		this.aid = aid;
	}
	public String getAns() {
		return ans;
	}
	public void setAns(String ans) {
		this.ans = ans;
	}
	public Question getQuestion() {
		return question;
	}
	public void setQuestion(Question question) {
		this.question = question;
	}
	@Override
	public String toString() {
		return "Answer [aid=" + aid + ", ans=" + ans + ", question=" + question + "]";
	}
	

}
