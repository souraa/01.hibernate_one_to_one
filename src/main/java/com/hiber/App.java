package com.hiber;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    Question q= new Question();
    q.setQid(1);
    q.setQues("What is your name");
    
    Answer a= new Answer();
    a.setAid(10);
    a.setAns("my name is Bikus");
    
    a.setQuestion(q);
    q.setAnswer(a);
    
    
       	
    	Configuration cf= new Configuration();
    	cf.configure("hibernate.cfg.xml");
    	SessionFactory sf=cf.buildSessionFactory();
    	Session session=sf.openSession();
    	Transaction tx=session.beginTransaction();
    	session.save(q);
    	session.save(a);
    	tx.commit();
    	session.close();
    }
}
