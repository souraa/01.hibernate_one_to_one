package com.hiber;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Question {
	@Id
	public int qid;
	public String ques;
	@OneToOne
	public Answer answer;
	public Question() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Question(int qid, String ques, Answer answer) {
		super();
		this.qid = qid;
		this.ques = ques;
		this.answer = answer;
	}
	public int getQid() {
		return qid;
	}
	public void setQid(int qid) {
		this.qid = qid;
	}
	public String getQues() {
		return ques;
	}
	public void setQues(String ques) {
		this.ques = ques;
	}
	public Answer getAnswer() {
		return answer;
	}
	public void setAnswer(Answer answer) {
		this.answer = answer;
	}
	@Override
	public String toString() {
		return "Question [qid=" + qid + ", ques=" + ques + ", answer=" + answer + "]";
	}
	
	
	
	
	
	

}
